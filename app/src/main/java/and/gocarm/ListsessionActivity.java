package and.gocarm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

import static and.gocarm.R.id.imageView4;


/**
 * Created by a on 10/19/2017.
 */

public class ListsessionActivity extends AppCompatActivity {

    private ListView listview_imageview;
    ArrayList<String> imagenumber = new ArrayList<>();
    NumbersAdapter adapter;
    Bitmap myBitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listsession);

        File yourDir = new File(getCacheDir().toString());
        if (yourDir.listFiles() != null) {

            for (File f : yourDir.listFiles()) {
                if (f.isFile()) {
                    myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath().toString());
                }
            }
        }

        listview_imageview = (ListView) findViewById(R.id.listview_imageview);

        adapter = new NumbersAdapter(this, imagenumber);
        listview_imageview.setAdapter(adapter);






    }

    // adapter class
    public class NumbersAdapter extends ArrayAdapter<String> {

        public NumbersAdapter(Context context, ArrayList<String> users) {
            super(context, 0, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {


            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_listsession_number, parent, false);

            ImageView item_number = (ImageView) convertView.findViewById(imageView4);
            item_number.setImageBitmap(myBitmap);

//            ImageView item_number1 = (ImageView) convertView.findViewById(imageView5);
//            item_number1.setImageBitmap(ViewcontrollerActivity.workBmp);

            return convertView;

        }

    }





}
