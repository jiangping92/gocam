package and.gocarm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * Created by a on 10/19/2017.
 */

public class RegisterActivity extends AppCompatActivity {
    private EditText txtEmailAddress;
    private EditText txtPassword;
    private EditText txtName;
    private EditText txtConfirmpassword;
    private FirebaseAuth firebaseAuth;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        txtEmailAddress = (EditText) findViewById(R.id.txtEmailRegistration1);
        txtPassword = (EditText) findViewById(R.id.txtPasswordRegistration1);
        txtName = (EditText) findViewById(R.id.name);
        firebaseAuth = FirebaseAuth.getInstance();
        txtConfirmpassword = (EditText)findViewById(R.id.confirmpassword);
    }

    public void Register(View v){
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String email = txtEmailAddress.getText().toString().trim();
        final String name = txtName.getText().toString();

        if (txtName.getText().toString().isEmpty() || txtEmailAddress.getText().toString().isEmpty() || txtPassword.getText().toString().isEmpty() || txtConfirmpassword.getText().toString().isEmpty()) {
            Toast.makeText(RegisterActivity.this,"Please fill all fields.",Toast.LENGTH_LONG).show();
            return;
        }
        if (email.matches(emailPattern))
        {
            Toast.makeText(getApplicationContext(),"valid email address",Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Invalid email address", Toast.LENGTH_SHORT).show();
        }



        if (txtPassword.getText().toString().length() < 6 || txtConfirmpassword.getText().toString().length() < 6) {
            Toast.makeText(RegisterActivity.this,"The password must be 6 characters long or more.",Toast.LENGTH_LONG).show();
            return;
        }
        if (!txtPassword.getText().toString().equalsIgnoreCase(txtConfirmpassword.getText().toString())){
            Toast.makeText(RegisterActivity.this,"Password do not match.",Toast.LENGTH_LONG).show();
            return;
        }

        final ProgressDialog progressDialog = ProgressDialog.show(RegisterActivity.this,"Please wait....","proccessing...",true);
        (firebaseAuth.createUserWithEmailAndPassword(txtEmailAddress.getText().toString(),txtPassword.getText().toString()))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if (task.isSuccessful()){
                            Toast.makeText(RegisterActivity.this,"Registration successful",Toast.LENGTH_LONG).show();

                            String uid = firebaseAuth.getCurrentUser().getUid();
                            FirebaseDatabase.getInstance().getReference().child("users").child(uid).child("name").setValue(name);

                            Intent i = new Intent(RegisterActivity.this,HomeActivity.class);
                            startActivity(i);
                        }
                        else
                        {
                            Log.e("ERROR",task.getException().toString());
                            Toast.makeText(RegisterActivity.this,task.getException().getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }
                });


        }

}

