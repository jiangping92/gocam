package and.gocarm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;
import android.util.Log;
import android.widget.Toast;

import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kim on 11/12/17.
 */

public class ImageHelper {

    private static final int threashold = 10;

    public static Bitmap findDifference(Context c, Bitmap firstImage, Bitmap secondImage)
    {
        if (firstImage.getHeight() != secondImage.getHeight() || firstImage.getWidth() != secondImage.getWidth())
            Toast.makeText(c, "Images size are not same", Toast.LENGTH_LONG).show();

        boolean isSame = true;

        Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
        Bitmap bmp = Bitmap.createBitmap(firstImage.getWidth(), firstImage.getHeight(), conf);

        for (int i = 0; i < firstImage.getWidth(); i++)
        {
            for (int j = 0; j < firstImage.getHeight(); j++)
            {
                int pixel = firstImage.getPixel(i,j);
                int redValue = Color.red(pixel);
                int blueValue = Color.blue(pixel);
                int greenValue = Color.green(pixel);

                int pixel2 = secondImage.getPixel(i,j);
                int redValue2 = Color.red(pixel2);
                int blueValue2 = Color.blue(pixel2);
                int greenValue2 = Color.green(pixel2);

                if (Math.abs(redValue2 - redValue) + Math.abs(blueValue2 - blueValue) + Math.abs(greenValue2 - greenValue) <= threashold)
//                if (firstImage.getPixel(i,j) == secondImage.getPixel(i,j))
                {
                }
                else
                {
//                    differentPixels.add(new Pixel(i,j));
                    bmp.setPixel(i,j, Color.YELLOW); //for now just changing difference to yello color
                    isSame = false;
                }
            }
        }

        return bmp;
    }

    public static PointF findShot( Bitmap bmp ) {
        Mat src = new Mat();
        Utils.bitmapToMat(bmp, src);
        Mat gray = new Mat();
        Imgproc.cvtColor(src, gray, Imgproc.COLOR_RGBA2GRAY);

        Imgproc.Canny(gray, gray, 50, 200);
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();

        //Bitmap result = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), Bitmap.Config.ARGB_8888);
        //Mat dest = new Mat();
        //Utils.bitmapToMat(result, dest);

        Imgproc.findContours(gray, contours, hierarchy, Imgproc.RETR_TREE,Imgproc.CHAIN_APPROX_SIMPLE);
        if (contours.size() == 0) {
            return new PointF(bmp.getWidth() / 2, bmp.getHeight() / 2);
        }

        int maxp = 0, maxi = 0;
        for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++) {
            if (contours.get(contourIdx).toArray().length > maxp) {
                maxp = contours.get(contourIdx).toArray().length;
                maxi = contourIdx;
            }
           // Imgproc.drawContours(dest, contours, contourIdx, new Scalar(0, 255, 0), -1);
        }
        //Imgproc.drawContours(dest, contours, maxi, new Scalar(255, 255, 0), -1);

        //Log.d("ABC", Integer.toString(maxi));
        //Utils.matToBitmap(dest, result);
        Point[] maxps = contours.get(maxi).toArray();
        if (maxps.length == 0) {
            return new PointF(bmp.getWidth() / 2, bmp.getHeight() / 2);
        }

        double xmin = maxps[0].x, ymin = maxps[0].y, xmax = maxps[0].x, ymax = maxps[0].y;
        for (int i = 0; i < maxps.length; i++) {
            if (xmin > maxps[i].x) xmin = maxps[i].x;
            if (ymin > maxps[i].y) ymin = maxps[i].y;
            if (xmax < maxps[i].x) xmax = maxps[i].x;
            if (ymax < maxps[i].y) ymax = maxps[i].y;
        }

        PointF p = new PointF((float) (xmax + xmin) / 2, (float) (ymax + ymin) / 2);
        return p;
    }


}
