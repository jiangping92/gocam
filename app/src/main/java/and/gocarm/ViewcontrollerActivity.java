package and.gocarm;

import android.app.Activity;

import android.content.Intent;

import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.graphics.Bitmap;

import org.apache.commons.io.FileUtils;

import java.io.IOException;
import java.io.File;

import java.util.ArrayList;
import java.util.List;

import and.gocarm.view.ZoomLayout;
import android.os.Handler;


/**
 * Created by a on 10/19/2017.
 */

public class ViewcontrollerActivity extends Activity
        implements TextureView.SurfaceTextureListener, ZoomLayout.OnZoomableLayoutClickEventListener{

    private MediaPlayer mMediaPlayer;
    private TextureView mPreview;
    private Surface surface;
    private LinearLayout layoutImages;
//    private List<Shot> listShots = new ArrayList<>();
    private int mIndex = 0;

    String TAG = "RTSP";
    String rtspurl = "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov";

    public static Bitmap workBmp = null;
    public static Bitmap firstBmp = null;

    public static int ACTIVITY_CHECKMARK_RESULT = 100;
    //recycle view
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<String> mDataset;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewcontroller);

        mPreview =  findViewById(R.id.rtspVideo);
        mPreview.setSurfaceTextureListener(this);
        layoutImages =  findViewById(R.id.linear);

//        File yourDir = new File(getCacheDir().toString());
//        if (yourDir.listFiles() != null) {
//            for (File f : yourDir.listFiles()) {
//                if (f.isFile()) {
//                    addImageToLayout(R.id.linear, f.getAbsoluteFile().toString());
//                }
//            }
//        }
        //Recycler view



        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MainAdapter();
        mRecyclerView.setAdapter(mAdapter);


    }



    private void RtspStream(String rtspurl){
        if(mPreview.isAvailable()){
            try{
                mMediaPlayer = new MediaPlayer();
                mMediaPlayer.setDataSource(this,Uri.parse(rtspurl));
                mMediaPlayer.setSurface(surface);
                mMediaPlayer.setLooping(true);
                mMediaPlayer.prepareAsync();
                mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {

                        mediaPlayer.start();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                firstBmp = getBitmap();
                                ((MainAdapter)mAdapter).addBitMap(firstBmp);
                               // addImage2List(firstBmp);
                            }
                        }, 2000);

                    }
                });



            }catch (IllegalArgumentException | SecurityException | IllegalStateException | IOException e){
                Log.d(TAG,e.getMessage());
            }
        }
    }








    public Bitmap getBitmap(){
        return mPreview.getBitmap(512,388);
    }
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        surface = new Surface(surfaceTexture);
        RtspStream(rtspurl);

    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

    }

    public void onClickback(android.view.View v) {

        finish();

    }
    public void onClickcapture(android.view.View v) {

        workBmp = getBitmap();

        Intent i = new Intent(this, CheckmarkActivity.class);
        i.putExtra("INDEX", mIndex);
        startActivityForResult(i, ACTIVITY_CHECKMARK_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ACTIVITY_CHECKMARK_RESULT) {
            if(resultCode == Activity.RESULT_OK){
               // addImage2List(workBmp);
                ((MainAdapter)mAdapter).addBitMap(workBmp);
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

   // private void addImage2List(Bitmap image) {

      //  Shot s = new Shot();
      //  s.bmp = image;
      //  s.index = mIndex++;
       // listShots.add(s);
      //  addImageToLayout(s.bmp);

//        File pictureFile = getOutputMediaFile();
//
//        if (pictureFile == null) {
//            Log.d(TAG,
//                    "Error creating media file, check storage permissions: ");// e.getMessage());
//            return;
//        }
//        try {
//            FileOutputStream fos = new FileOutputStream(pictureFile);
//            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
//            fos.close();
//
//            addImageToLayout(R.id.linear, pictureFile.getAbsoluteFile().toString());
//
//
//
//        } catch (FileNotFoundException e) {
//            Log.d(TAG, "File not found: " + e.getMessage());
//        } catch (IOException e) {
//            Log.d(TAG, "Error accessing file: " + e.getMessage());
//        }

    //}
    // add images to horizontal scrollview

//    private ImageView addImageToLayout(Bitmap bmp) {
//        ImageView imageView = new ImageView(this);
//        imageView.setPadding(4, 4, 4, 4);
//        imageView.setImageBitmap(bmp);
//        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//        layoutImages.addView(imageView);
//        imageView.getLayoutParams().width = 600;
//        return imageView;
//    }

//    private void addImageToLayout(int layout_id, String fname) {
//        LinearLayout layout = (LinearLayout) findViewById(layout_id);
//        ImageView imageView = new ImageView(this);
//        imageView.setPadding(4, 4, 4, 4);
//        imageView.setImageBitmap(BitmapFactory.decodeFile(fname));
//        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//        layout.addView(imageView);
//        imageView.getLayoutParams().width = 600;
//    }

    // remove in horizontal view

   // private void removeImageInLayout(int layout_id) {
//        LinearLayout layout = (LinearLayout) findViewById(layout_id);
//
//        layout.removeAllViews();


   // }

//    File getOutputMediaFile(){
//
//        File mediaStorageDir = new File(getCacheDir().toString());
//
//
//
//        if (! mediaStorageDir.exists()){
//            if (! mediaStorageDir.mkdirs()){
//                return null;
//            }
//        }
//        // Create a media file name
//        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
//        File mediaFile;
//        String mImageName="MI_"+ timeStamp +".jpg";
//        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
//        Log.d("File", mediaFile.getAbsolutePath());
//
//        return mediaFile;
//    }
    //restart
    public void Restart(android.view.View v){
       // File yourDir = new File(getCacheDir().toString());
        //removeImageInLayout(R.id.linear);
        ((MainAdapter)mAdapter).clear();
        firstBmp = getBitmap();
        ((MainAdapter)mAdapter).addBitMap(firstBmp);

//        try {
//            FileUtils.deleteDirectory(yourDir);
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    //seve event
    public void onSave(android.view.View v) {

        finish();
    }

    // create directory
//    File createDirectory(){
//        int ddd = 1;
//        String count = String.format("%3d", ddd);
//        String timeStamp = new SimpleDateFormat("dd/MM/yyyy ").format(new Date());
//        File mydir = getBaseContext().getDir("timeStamp"+" "+count,Context.MODE_PRIVATE);
//
//        if (! mydir.exists()){
//            if (! mydir.mkdirs()){
//                return null;
//            }
//        }
//        File mediaFile;
//        String mImageName="MI_"+ timeStamp +".jpg";
//        mediaFile = new File(mydir.getPath() + File.separator + mImageName);
//        Log.d("File", mediaFile.getAbsolutePath());
//
//        return mediaFile;
//
//    }

    @Override
    public void OnContentClickEvent(int action, float xR, float yR) {

    }



//    public class Shot {
//        Bitmap bmp;
//        int index;
//    }
//




}
