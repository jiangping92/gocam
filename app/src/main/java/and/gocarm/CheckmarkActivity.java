package and.gocarm;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import and.gocarm.view.ZoomLayout;


/**
 * Created by a on 10/31/2017.
 */

public class CheckmarkActivity extends AppCompatActivity implements ZoomLayout.OnZoomableLayoutClickEventListener  {

    int idx= 0;
    TextView tv;
    RelativeLayout relativeLayout_work;
    float prevX = 0, prevY = 0;
    Bitmap test_1;
    Bitmap test_2;
    Bitmap test_3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_checkmark);
            //addImageToImageview();

            idx = getIntent().getIntExtra("INDEX", 1);
            tv = (TextView)findViewById(R.id.tv);
            tv.setText(idx+"");
            relativeLayout_work = (RelativeLayout)findViewById(R.id.relativeLayout_work);

//            Bitmap test_1 = BitmapFactory.decodeResource(CheckmarkActivity.this.getResources(), R.drawable.test_1);
//            Bitmap test_2 = BitmapFactory.decodeResource(CheckmarkActivity.this.getResources(), R.drawable.test_2);
   }

   // image compare
   class TestAsync extends AsyncTask<Object, Integer, String>
   {
       String TAG = getClass().getSimpleName();
       PointF p;

       protected void onPreExecute (){
           super.onPreExecute();
           Log.d(TAG + " PreExceute","On pre Exceute......");
       }

       protected String doInBackground(Object...arg0) {

          // Bitmap test_1 = BitmapFactory.decodeResource(CheckmarkActivity.this.getResources(), R.drawable.test_1);
           //Bitmap test_2 = BitmapFactory.decodeResource(CheckmarkActivity.this.getResources(), R.drawable.test_2);
           test_1 = ViewcontrollerActivity.firstBmp;
           test_2 = ViewcontrollerActivity.workBmp;


           test_3 = ImageHelper.findDifference(CheckmarkActivity.this, test_1, test_2);

           //Bitmap tmp = ImageHelper.findDifference(CheckmarkActivity.this, test_1, test_2);

           publishProgress(10);
           p = ImageHelper.findShot(test_3);

          // ViewcontrollerActivity.workBmp = ImageHelper.findShot(tmp);
           //ViewcontrollerActivity.workBmp = tmp;
           test_1 = test_2;

           return "You are at PostExecute";
       }

       protected void onProgressUpdate(Integer...a){
           super.onProgressUpdate(a);
           Log.d(TAG + " onProgressUpdate", "You are in progress update ... " + a[0]);
       }

       protected void onPostExecute(String result) {
           super.onPostExecute(result);
           addImageToImageview();
           p.x = p.x * relativeLayout_work.getWidth() / ViewcontrollerActivity.workBmp.getWidth() - tv.getWidth() / 2;
           p.y = p.y * relativeLayout_work.getHeight() / ViewcontrollerActivity.workBmp.getHeight() - tv.getHeight() / 2;
           //movePointTo(p.x, p.y);
           tv.setX(p.x);
           tv.setY(p.y);
           Log.d(TAG + " onPostExecute","On post Exceute......");
       }
   }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i("OpenCV", "OpenCV loaded successfully");
                    new TestAsync().execute();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d("OpenCV", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }



    private void addImageToImageview(){


        if (ViewcontrollerActivity.workBmp != null) {
            ImageView myImage = (ImageView)findViewById(R.id.imageViewtest);
            myImage.setImageBitmap(ViewcontrollerActivity.workBmp);
        }


    }
    // viewcontrol label
    public void tv(){

        TextView tv = (TextView)findViewById(R.id.tv);

        tv.setText(idx+"");
    }

    public void onConfirm(View v){
        View vv = findViewById(R.id.relativeLayout_work);
        ViewcontrollerActivity.workBmp = viewToBitmap(vv, vv.getWidth(), vv.getHeight());
        setResult(Activity.RESULT_OK);
        finish();
    }

    public void onCancel(View v){
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    @Override
    public void onBackPressed() {

        setResult(Activity.RESULT_CANCELED);

        finish();

    }

    public static Bitmap viewToBitmap(View view, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }
    // touch
    private void movePointTo(float xR, float yR) {
        AnimationSet aset = new AnimationSet(true);
        TranslateAnimation anit = new TranslateAnimation(prevX, xR - tv.getX(), prevY, yR - tv.getY());
        prevX = xR - tv.getX();
        prevY = yR - tv.getY();
        aset.addAnimation(anit);
        aset.setFillAfter(true);
        tv.startAnimation(aset);
    }


    @Override
    public void OnContentClickEvent(int action, float xR, float yR) {
        movePointTo(xR, yR);
    }

    public void onMoveButtonPressed(View v) {
        ImageButton b = (ImageButton)v;

        int mId = b.getId();

        switch (mId) {
            case R.id.imageButtonL:
                movePointTo(prevX + tv.getX() - 1, prevY + tv.getY());
                break;
            case R.id.imageButtonR:
                movePointTo(prevX + tv.getX() + 1, prevY + tv.getY());
                break;
            case R.id.imageButtonD:
                movePointTo(prevX + tv.getX(), prevY + tv.getY() + 1);
                break;
            case R.id.imageButtonU:
                movePointTo(prevX + tv.getX(), prevY + tv.getY() - 1);
                break;
        }

    }

}
