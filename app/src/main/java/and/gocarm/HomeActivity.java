package and.gocarm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by a on 10/19/2017.
 */

public class HomeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
    public void onClickabout(android.view.View v) {

        startActivity(new Intent(this, AboutActivity.class));

    }
    public void onClickprevious(android.view.View v) {

        startActivity(new Intent(this, ListsessionActivity.class));

    }
    public void onClicknewseesion(android.view.View v) {

        startActivity(new Intent(this, ViewcontrollerActivity.class));

    }
    public void logout(android.view.View v) {

        FirebaseAuth.getInstance().signOut();
        finish();

    }
}
