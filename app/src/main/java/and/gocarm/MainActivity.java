package and.gocarm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


import static and.gocarm.R.styleable.View;

public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("native-lib");
    }

    private EditText txtEmailAddress;
    private EditText txtPassword;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txtEmailAddress = (EditText) findViewById(R.id.txtEmailRegistration);
        txtPassword = (EditText) findViewById(R.id.txtPasswordRegistration);
        firebaseAuth = FirebaseAuth.getInstance();

        if (firebaseAuth.getCurrentUser() != null) {
            gotoHome();
        }
    }

    public void onClicklogin(android.view.View v) {

        if (txtEmailAddress.getText().toString().isEmpty() || txtPassword.getText().toString().isEmpty()) {
            Toast.makeText(MainActivity.this,"Please fill all fields.",Toast.LENGTH_LONG).show();
            return;
        }

        if (txtPassword.getText().toString().length() < 6) {
            Toast.makeText(MainActivity.this,"The password must be 6 characters long or more.",Toast.LENGTH_LONG).show();
            return;
        }

        final ProgressDialog progressDialog = ProgressDialog.show(MainActivity.this,"Please wait....","proccessing...",true);

        (firebaseAuth.signInWithEmailAndPassword(txtEmailAddress.getText().toString(),txtPassword.getText().toString()))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();

                        if (task.isSuccessful()){
                            Toast.makeText(MainActivity.this,"Login successful",Toast.LENGTH_LONG).show();

                            gotoHome();

                           // startActivity(new Intent(this, HomeActivity.class));
                        }else{
                            Log.e("ERROR",task.getException().toString());
                            Toast.makeText(MainActivity.this,task.getException().getMessage(),Toast.LENGTH_LONG).show();

                        }

                    }
                });
    }

    public void onClickforgot(android.view.View v) {

        startActivity(new Intent(this, ForgotActivity.class));

    }
    public void onClickregister(android.view.View v) {

        startActivity(new Intent(this, RegisterActivity.class));

    }

    private void gotoHome() {
        Intent i = new Intent(MainActivity.this,HomeActivity.class);
        i.putExtra("Email",firebaseAuth.getCurrentUser().getEmail());
        startActivity(i);
    }

    public native String stringFromJNI();
}
