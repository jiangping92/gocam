package and.gocarm;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by a on 10/19/2017.
 */

public class ForgotActivity extends AppCompatActivity {
    private EditText textEmail;
    private static final String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        textEmail = (EditText)findViewById(R.id.forgotemail);

    }
    public void onClickforgot (View v){

        FirebaseAuth auth = FirebaseAuth.getInstance();
        String emailAddress = textEmail.getText().toString();
        if(emailAddress.isEmpty()){
            Toast.makeText(ForgotActivity.this,"Please enter your email address.",Toast.LENGTH_LONG).show();
            return;
        }
        auth.sendPasswordResetEmail(emailAddress)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Email sent.");
                            Toast.makeText(ForgotActivity.this,"Forgot password request sent.",Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(ForgotActivity.this,task.getException().getMessage().toString(),Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }

}
