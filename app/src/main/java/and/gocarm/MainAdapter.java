package and.gocarm;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

/**
 * Created by a on 11/17/2017.
 */

class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    private ArrayList<String> mDataset;
    private ArrayList<Bitmap> mImageset;

   public MainAdapter() {
        this.mDataset = new ArrayList<String>();
        this.mImageset = new ArrayList<Bitmap>();
    }

    public void addBitMap(Bitmap bmp) {
        int count = this.mDataset.size();
        mDataset.add(0,String.valueOf(count + 1));
        mImageset.add(0,bmp);
        this.notifyDataSetChanged();
    }
    //restart
    public void clear() {
        mDataset.clear();
        mImageset.clear(); //clear list
        this.notifyDataSetChanged(); //let your adapter know about the changes and reload view.
    }


    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(MainAdapter.ViewHolder holder, int position) {
        holder.mTitle.setText(mDataset.get(position));
        holder.mImage.setImageBitmap(mImageset.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView mTitle;
        public ImageView mImage;
        public ViewHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.title);
            mImage = itemView.findViewById(R.id.capturedImage);
        }
    }
}
